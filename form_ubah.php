<html>
<head>
	<title>Ubah Data</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.min.css>">
    <link href="https://fonts.googleapis.com/css?family=Lato:300|Oswald:200,300,400,500,600,700" rel="stylesheet">
</head>
<body>
	
	<div class="container">
	<div class="thumbnail" style="background-color:rgba(255,255,255,.5);">
	<div class="row"  style="margin-top:40px">
	<h1 class="text-center">Ubah Data Barang</h1>
	<div class="col-md-8 col-md-offset-4">
	<?php
	include "koneksi.php";

	$id = $_GET['id'];
	
	$query = "SELECT * FROM items WHERE id='".$id."'";
	$sql = mysqli_query($connect, $query);
	$data = mysqli_fetch_array($sql);
	?>
	
	<form method="post" action="proses_ubah.php?id=<?php echo $id; ?>" enctype="multipart/form-data">
	<table cellpadding="8">
	<tr>
		<td>Nama Barang</td>
		<td><input type="text" name="itemname" value="<?php echo $data['itemname']; ?>"></td>
	</tr>
	<tr>
		<td>Harga Beli</td>
		<td><input type="text" name="buyprice" value="<?php echo $data['buyprice']; ?>"></td>
	</tr>
	<tr>
		<td>Harga Jual</td>
		<td><input type="text" name="sellprice" value="<?php echo $data['sellprice']; ?>"></td>
	</tr>
	<tr>
		<td>Stock</td>
		<td><input type="text" name="stock" value="<?php echo $data['stock']; ?>"></td>
	</tr>
	<tr>
		<td>Foto</td>
		<td>
			<input type="checkbox" name="ubah_foto" value="true"> Ceklis jika ingin mengubah foto<br>
			<input type="file" name="itemimg">
		</td>
	</tr>
	</table>
	
	<hr>
	<input type="submit" value="Ubah">
	<a href="index.php"><input type="button" value="Batal"></a>
	</form>
</div>
</div>
</div>
</body>

<script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/sweetalert2.min.js"></script>

</html>
