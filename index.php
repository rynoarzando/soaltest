<!DOCTYPE html>
<html>
	
	<head>
		<title>
			Soal Test Praktek PHP Programmer
		</title>
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/style.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/dataTables.bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.min.css" />
    	<link href="https://fonts.googleapis.com/css?family=Lato:300|Oswald:200,300,400,500,600,700" rel="stylesheet" />
	</head>
	<body>
		<h1 class="text-center">
			Data Barang
		</h1>
			<div class="container">
				<button class="btn btn-default" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i> Buat Baru</button><br>
				<table id="bebas" class=" table display">
					<thead>
						<tr>
							<th>Gambar</th>
							<th>Nama Barang</th>
							<th>Harga Jual</th>
							<th>Harga Beli</th>
							<th>Stock</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<?php

							include "koneksi.php";
		
							$query = "SELECT * FROM items";
							$sql = mysqli_query($connect, $query);
		
							while($data = mysqli_fetch_array($sql)){
								echo "<tr>
								";
								echo "<td><img src='images/".$data['itemimg']."' width='100' height='100' ></td>
								";
								echo "<td>".$data['itemname']."</td>
								";
								echo "<td>".$data['buyprice']."</td>
								";
								echo "<td>".$data['sellprice']."</td>
								";
								echo "<td>".$data['stock']."</td>
								";
								echo "<td><a href='form_ubah.php?id=".$data['id']."'>Ubah</a></td>
								";
								echo "<td><a href='proses_hapus.php?id=".$data['id']."'>Hapus</a></td>
								";
								echo "</tr>
								";
							}
							?>
						
					</tbody>
				</table>	
			</div>

		<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<form action="proses_simpan.php" method="post" enctype="multipart/form-data" >
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

							<div class="modal-body">
								<h1 class="text-center">Tambah Data Barang</h1>
									<table cellpadding="8">
										<tr>
											<td>Nama Barang</td>
											<td>
												<input type="text" name="itemname">
											</td>
										</tr>
										<tr>
											<td>Harga Beli</td>
											<td>
												<input type="number" name="buyprice">
											</td>
										</tr>
										<tr>
											<td>Harga jual</td>
											<td>
												<input type="number" name="sellprice">
											</td>
										</tr>
										<tr>
											<td>Stock</td>
											<td>
												<input type="number" name="stock" />
											</td>
										</tr>
										<tr>
											<td>Foto</td>
											<td>
												<input type="file" name="foto" />
											</td>
										</tr>
									</table>
									<hr>
									<input type="submit" value="Simpan" />
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>


	
	
	<script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="assets/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/sweetalert2.min.js"></script>
	<script>
			$('#bebas').dataTable();
	</script>
	</body>

</html>